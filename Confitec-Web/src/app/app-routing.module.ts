import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonUserAddComponent } from './feature/person-user-feature/person-user-add/person-user-add.component';
import { PersonUserEditComponent } from './feature/person-user-feature/person-user-edit/person-user-edit.component';
import { PersonUserComponent } from './feature/person-user-feature/person-user/person-user.component';


const routes: Routes = [
  { path: '', component: PersonUserComponent },
  { path: 'novo', component: PersonUserAddComponent },
  { path: 'editar/:id', component: PersonUserEditComponent },

];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
