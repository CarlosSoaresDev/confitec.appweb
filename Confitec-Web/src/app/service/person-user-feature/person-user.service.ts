import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs'
import { take } from 'rxjs/operators';
import { IPersonUserService } from 'src/app/domain/person-user-feature/iperson-user-service';
import { PersonUser } from 'src/app/domain/person-user-feature/person-user';
import { PaginatedContent } from 'src/app/domain/suport-feature/paginated-content';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export abstract class PersonUserService implements IPersonUserService {

  constructor(
    private http: HttpClient,
    private toastrService: ToastrService
  ) {  }

  Create(model: PersonUser): void {
    this.http
      .post(`${environment.URL_API}/api/PersonUser`, model)
      .pipe(take(1))
      .subscribe(() => {
        this.toastrService.success('Adicionado com sucesso')
      }, error => {
        this.toastrService.error('Ouve um erro')
      });;
  }

  DeleteById(id: number): boolean {
    let isSucess = false

    this.http
      .delete(`${environment.URL_API}/api/PersonUser/${id}`)
      .pipe(take(1))
      .subscribe(() => {
        this.toastrService.success('Deletado com sucesso')
        isSucess = true;
      }, error => {
        this.toastrService.error('Ouve um erro')
        isSucess = false
      });

    return isSucess;
  }

  GetAll(query: string): Observable<PaginatedContent<PersonUser>> {
    return this.http
      .get<PaginatedContent<PersonUser>>(`${environment.URL_API}/api/PersonUser?query=${query}`);
  }

  GetById(id: number): Observable<PersonUser> {
    return this.http.get<PersonUser>(`${environment.URL_API}/api/PersonUser/${id}`)
  }

  Update(model: PersonUser, id: number): void {
    this.http
      .put(`${environment.URL_API}/api/PersonUser/${id}`, model)
      .pipe(take(1))
      .subscribe(() => {
        this.toastrService.success('Atualizado com sucesso')
      }, error => {
        this.toastrService.error('Ouve um erro')
      });
  }
}
