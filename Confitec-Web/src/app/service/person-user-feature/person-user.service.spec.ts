import { TestBed } from '@angular/core/testing';

import { PersonUserService } from './person-user.service';

describe('PersonUserService', () => {
  let service: PersonUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PersonUserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
