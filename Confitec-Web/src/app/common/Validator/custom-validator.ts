import { FormControl } from '@angular/forms';

export function BirthDateSmallerThanToday(control: FormControl): {
    [key: string]:
    boolean
} | null {
    const value = new Date(control.value);
    const date = new Date();

    if (value >= date) {
        return { birthdatesmallerthantoday: true };
    } else {
        return null;
    }
}
