export function formatDate(dateValue: Date) {
    const date = new Date(dateValue);

    const day = date.getDate().toString(),
        dayResult = (day.length === 1) ? '0' + day : day,
        month = (date.getMonth() + 1).toString(),
        monthResult = (month.length === 1) ? '0' + month : month,
        yearResult = date.getFullYear();
    const result = `${yearResult}-${monthResult}-${dayResult}`;

    return result;
}