import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BirthDateSmallerThanToday } from 'src/app/common/Validator/custom-validator';
import { DictonaryEducation, EnumEducation } from 'src/app/domain/enum/enum-education';
import { IPersonUserService } from 'src/app/domain/person-user-feature/iperson-user-service';
import { PersonUserService } from 'src/app/service/person-user-feature/person-user.service';

@Component({
  selector: 'app-person-user-add',
  templateUrl: './person-user-add.component.html',
  styleUrls: ['./person-user-add.component.css']
})
export class PersonUserAddComponent implements OnInit {
  private readonly _personUserService: IPersonUserService;
  registerForm!: FormGroup;
  submitted = false;
  loading = false;

  dictionaryEducation = DictonaryEducation;
  educations = Object.values(EnumEducation).filter(value => !isNaN(Number(value)));

  constructor(
    private personUserService: PersonUserService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this._personUserService = this.personUserService;
  }

  ngOnInit(): void {
    this.formulary()
  }

  formulary() {
    this.registerForm = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(255)])],
      nickName: [null, Validators.maxLength(100)],
      email: [null, Validators.compose([Validators.required, Validators.email])],
      birthday: [null, Validators.compose([Validators.required, BirthDateSmallerThanToday])],
      education: [null, Validators.compose([Validators.required])]
    });
  }

  get form() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.form.education.setValue(Number(this.form.education.value));

    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.loading = true;
    this._personUserService.Create(this.registerForm.value);
    this.router.navigate(['']);
    this.loading = false;
  }
}
