import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonUserAddComponent } from './person-user-add.component';

describe('PersonUserAddComponent', () => {
  let component: PersonUserAddComponent;
  let fixture: ComponentFixture<PersonUserAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonUserAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonUserAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
