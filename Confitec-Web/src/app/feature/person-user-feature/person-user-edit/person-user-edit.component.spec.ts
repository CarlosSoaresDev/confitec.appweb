import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonUserEditComponent } from './person-user-edit.component';

describe('PersonUserEditComponent', () => {
  let component: PersonUserEditComponent;
  let fixture: ComponentFixture<PersonUserEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonUserEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonUserEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
