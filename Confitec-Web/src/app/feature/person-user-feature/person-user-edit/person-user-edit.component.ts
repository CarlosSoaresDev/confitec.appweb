import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from 'src/app/common/formart/date-formater';

import { BirthDateSmallerThanToday } from 'src/app/common/Validator/custom-validator';
import { DictonaryEducation, EnumEducation } from 'src/app/domain/enum/enum-education';
import { IPersonUserService } from 'src/app/domain/person-user-feature/iperson-user-service';
import { PersonUserService } from 'src/app/service/person-user-feature/person-user.service';

@Component({
  selector: 'app-person-user-edit',
  templateUrl: './person-user-edit.component.html',
  styleUrls: ['./person-user-edit.component.css']
})
export class PersonUserEditComponent implements OnInit {

  private readonly _personUserService: IPersonUserService;
 
  registerForm!: FormGroup;
  submitted = false;
  loading = false;
  id = 0;
  dictionaryEducation = DictonaryEducation;
  educations = Object.values(EnumEducation).filter(value => !isNaN(Number(value)));

  constructor(
    private personUserService: PersonUserService,
    private formBuilder: FormBuilder,
    private activatedRouter: ActivatedRoute,
    private router: Router
  ) {
    this._personUserService = this.personUserService;
    this.id = this.activatedRouter.snapshot.params.id;
    this._personUserService.GetById(this.id)
      .subscribe((s) => {
        this.registerForm = this.formBuilder.group({
          id: [s.id],
          name: [s.name, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(255)])],
          nickName: [s.nickName, Validators.maxLength(100)],
          email: [s.email, Validators.compose([Validators.required, Validators.email])],
          birthday: [formatDate(s.birthday), Validators.compose([Validators.required, BirthDateSmallerThanToday])],
          education: [s.education, Validators.compose([Validators.required])]
        });
      })
  }

  ngOnInit(): void {
    this.formulary()
  }

  formulary(): void {
    this.registerForm = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(255)])],
      nickName: [null, Validators.maxLength(100)],
      email: [null, Validators.compose([Validators.required, Validators.email])],
      birthday: [null, Validators.compose([Validators.required, BirthDateSmallerThanToday])],
      education: [null, Validators.compose([Validators.required])]
    });
  }

  get form() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.form.education.setValue(Number(this.form.education.value));

    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.loading = true;
    this._personUserService.Update(this.registerForm.value, this.id);
    this.router.navigate([''])
    this.loading = false;
  }
}

