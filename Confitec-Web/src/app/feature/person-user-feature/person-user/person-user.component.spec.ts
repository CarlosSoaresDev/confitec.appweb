import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonUserComponent } from './person-user.component';

describe('PersonUserComponent', () => {
  let component: PersonUserComponent;
  let fixture: ComponentFixture<PersonUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
