import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { IPersonUserService } from 'src/app/domain/person-user-feature/iperson-user-service';
import { PersonUser } from 'src/app/domain/person-user-feature/person-user';
import { PaginatedContent } from 'src/app/domain/suport-feature/paginated-content';
import { PersonUserService } from 'src/app/service/person-user-feature/person-user.service';

@Component({
  selector: 'app-person-user',
  templateUrl: './person-user.component.html',
  styleUrls: ['./person-user.component.css']
})
export class PersonUserComponent implements OnInit {

  private readonly _personUserService: IPersonUserService;

  filterForm!: FormGroup;
  personUsers$!: Observable<PaginatedContent<PersonUser>>;

  constructor(
    private personUserService: PersonUserService,
    private formBuild: FormBuilder
  ) {
    this._personUserService = this.personUserService;
  }

  ngOnInit(): void {
    this.filterForm = this.formBuild.group({
      search: [''],
      limit: [null],
      skip: [null]
    });

    this.GetAllPersonUsers();
  }

  GetAllPersonUsers(): void {
    this.personUsers$ = this._personUserService.GetAll(JSON.stringify(this.filterForm.value));
  }

  onDelete(id: number): void {
    if (!this._personUserService.DeleteById(id))
      this.GetAllPersonUsers();
  }
}
