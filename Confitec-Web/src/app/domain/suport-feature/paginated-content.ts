export class PaginatedContent<T>{

    public page!: number;
    public total!: number;
    public content!: Array<T>;
}