import { EntityBase } from "../suport-feature/entity-base";

export class PersonUser extends EntityBase {
    public name!: string ;
    public nickName!: string ;
    public email!: string ;
    public birthday!: Date ;
    public education!: Number ;
}
