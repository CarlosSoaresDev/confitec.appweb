import { Observable } from "rxjs";
import { PaginatedContent } from "../suport-feature/paginated-content";
import { PersonUser } from "./person-user";

export interface IPersonUserService {

    Create(model: PersonUser): void;

    DeleteById(id: number): boolean;

    GetAll(query: string): Observable<PaginatedContent<PersonUser>>;

    GetById(id: number): Observable<PersonUser>;

    Update(model: PersonUser, id: number): void;

}
