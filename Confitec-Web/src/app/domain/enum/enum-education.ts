export enum EnumEducation {
    kindergarten = 1,
    ElementarySchool = 2,
    HighSchool = 3,
    UniversityEducation = 4
}

export const DictonaryEducation: Record<string, string> = {
    [EnumEducation.kindergarten]: "Infantil",
    [EnumEducation.ElementarySchool]: "Ensino Fundamental",
    [EnumEducation.HighSchool]: "Ensino Medio",
    [EnumEducation.UniversityEducation]: "Ensino Superior"
};

export const DictonaryEducationConvert: Record<number, string> = {
    [EnumEducation.kindergarten]: "Infantil",
    [EnumEducation.ElementarySchool]: "Ensino Fundamental",
    [EnumEducation.HighSchool]: "Ensino Medio",
    [EnumEducation.UniversityEducation]: "Ensino Superior"
};